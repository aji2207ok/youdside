const express = require("express");
const router = express.Router()
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
// const port = 5000;
const port = process.env.PORT || 5000;

//import validation
const { registerValidation } = require('./configs/validation')

dotenv.config();

app.use(bodyParser.json());
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));


// const { urlencoded } = require("body-parser");
mongoose.Promise = global.Promise;
mongoose.connect(
  process.env.MDB_CONNECT || 5000,
  // "mongodb+srv://youds:qubisa@youdscluster.lch4i.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  // mongoose.connect("mongodb+srv://root:root@cluster0.prm7t.mongodb.net/postdata?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);  

// var nameSchema = new mongoose.Schema({
//   informasi: String,
// });
// var Data = mongoose.model("Data", nameSchema);

// ##Service##
var resultCaptureSchema = new mongoose.Schema({
  capture: Array,
  datetime: { type: Date, default: new Date() },
});

// var Data2 = mongoose.model("resultCapture", resultCaptureSchema);
var Data2 = mongoose.model("expressions", resultCaptureSchema);

// Data.find().exec(function (err, results) {
//   var count_index = results.length;
//   console.log(count_index);
// });

// var emotion_labels = [
//   "angry",
//   "disgust",
//   "fear",
//   "happy",
//   "sad",
//   "surprise",
//   "neutral",
// ];

// for (i = 0; i < emotion_labels.length; i++) {
//   //console.log(emotion_labels[i]);
//   Data.find({ posting: emotion_labels[i] }).exec(function (err, results) {
//     count_sad = results.length;
//     //globalCountSad = count_sad;
//   });
// }

// === schema and model user ===
const UserDB = mongoose.Schema({
  firstName: {
    type: String,
    require: [true, "Masukkan Nama Depan"]
  },

  lastName: {
    type: String,
    require: [true, "Masukkan Nama Belakang"]
  },
  
  email: {
    type: String,
    require: [true, "Masukkan Email"]
  },

  password: {
    type: String,
    require: [true, "Masukkan Password"]
  },

  company: {
    type: String,
    require: [true, "Masukkan Nama Perusahaan"]
  },

  country: {
    type: String,
    require: [true, "Masukkan Nama Negara"]
  }

});

const Users = mongoose.model('Users', UserDB);

app.get("/", (req, res) => {
  res.sendFile(path.resolve(__dirname, "qubisa_web", "build"));
  // res.jsonp();
});

app.listen(port, () => {
  console.log("Server listening on port " + port);
});

app.post('/user', async (req, res) => {
  //if email exist

  try {
    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(req.body.password, salt);

    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = passwordHash;
    const company = req.body.company;
    const country = req.body.country;

    // const { error } = registerValidation(req.body)
    // if(error) return res.status(400).json({
    //   status: res.statusCode,
    //   message: error.details[0].message
    // })

    //edit
    if (!email || !password)
    return res
    .status(400)
    .json({ errorMessage: "Please enter all required fields." });
        
    if (password.length < 6)
      return res
      .status(400)
      .json({
      errorMessage: "Please enter a password of at least 6 characters.",
      });
      
    
    const existingUser = await Users.findOne({ email });
    if (existingUser)
        return res.status(400).json({
          errorMessage: "An account with this email already exists.",
        });

        if(error) return res.status(400).json({
          errorMessage: "Please enter all required fields."
        });

    const adduser = new Users ({
        firstName,
        lastName,
        email,
        password,
        company,
        country
      });

  

  const savedUser = await adduser.save();
  res.json(savedUser)
  
  } catch (err) {
    res.status(400).json({
      status: res.statusCode,
      message: 'Gagal Membuat user baru'
  })
  }
});


app.post("/multiresult", (req, res) => {
  let dt_txt = req.body;
  const dataMulti = new Data2({
    capture: req.body.capture,
  });

  console.log(req);
  
  dataMulti
    .save()
    .then((item) => {
      console.log(res);
      res.status(201).json({
        message: "Data successfully save to database",
        body: req.body,
      });
    })
    .catch((err) => {
      res.status(500).send("Unable to save to database");
    });
});

if (process.env.NODE_ENV === "production") {
  app.use(express.static("qubisa_web/build"));
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "qubisa_web", "build", "index.html"));
  });
}

app.post("/adddata", (req, res) => {
  var myData = new Data(req.body);
  myData
    .save()
    .then((item) => {
      res.send("Name saved to database");
      console.log(res);
    })
    .catch((err) => {
      res.status(400).send("Unable to save to database");
    });
});



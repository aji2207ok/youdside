import {useState, useEffect} from 'react';
import axios from "axios";

const qs = require('query-string');

function Signup() {
  const [user, setUser] = useState([
    {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      company: '',
      country: ''
    }
  ])

  function handleChange(e) {
    const {name, value} = e.target;
    setUser(prevInput => {
      return(
        {
          ...prevInput,
          [name]: value
        }
      )
    })
  }

  // const salt = await bcrypt.genSalt();
  // const passwordHash = await bcrypt.hash(password, salt);

  function addUser(e){
    if (!user.this.email || !user.this.password){
      alert("Please enter all required fields.")
    }
    
    if (user.this.password.length < 6){   
    alert(
    "Please enter a password of at least 6 characters."
    )
    }

      e.preventDefault();
      // alert("User Added");
      const newUser = {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        password: user.password,
        company: user.company,
        country: user.country
      }
      
      
    var config = {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        responseType: 'blob'
    }; 

    // var url = 'https://youdsqubisa123.herokuapp.com/user'
    var url = 'http://localhost:5000/user'
    axios.post(url, newUser)
      .then((res) => {
        alert("Sign Up Success");
      
      })
      .catch((err) => {
        console.log(err);
      })
    // console.log(this.state);
    // axios.post('/user', newUser);
  }
  
    return (
    <center>
      <div className="tm-bg-white tm-call-to-action-text">
        <form className="needs-validation" noValidate>
          <div className="form-row">
              <div className="col-md-4 mb-3">
                <label>First name</label>
                <input onChange={handleChange} name="firstName" value={user.firstName} type="text" className="form-control text-dark" id="validationCustom01" placeholder="First name" required>
                </input>
              </div>  
              <div className="col-md-4 mb-3">
                <label>Last name</label>
                <input onChange={handleChange} name="lastName" value={user.lastName} type="text" className="form-control text-dark" id="validationCustom02" placeholder="Last name" required>
                </input>
              </div>
              <div className="col-md-4 mb-3">
                  <label>Email</label>
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text" id="inputGroupPrepend">@</span>
                    </div>
                    <input onChange={handleChange} name="email" value={user.email} type="text" className="form-control text-dark" id="validationCustomEmail" placeholder="Email" aria-describedby="inputGroupPrepend" required>
                    </input>  
                    </div>
                  </div>
              </div>

          <div className="form-row">
          <div className="col-md-4 mb-3">
                <label>Password</label>
                <input onChange={handleChange} name="password" value={user.password} type="password" className="form-control text-dark" id="validationCustom03" placeholder="Password" required>
                </input>
              </div>
              
            <div className="col-md-4 mb-3"> 
              <label>Company</label>
              <input onChange={handleChange} name="company" value={user.company} type="text" className="form-control text-dark" id="validationCustom04" placeholder="Company" required>
              </input>
            </div>
  
            <div className="col-md-4 mb-3">
              <label>Country</label>
              <input onChange={handleChange} name="country" value={user.country} type="text" className="form-control text-dark" id="validationCustom05" placeholder="Country" required>
              </input>
            </div>
          <div className="form-group">
            <button className="btn btn-secondary" type="submit" onClick={addUser}>submit form</button>
          </div>
          </div>
        </form >
      </div>
    </center>
    )
  
}

export default Signup;

// constructor(props){
  //   super(props);

  //   this.state = {
  //     firstName : '',
  //     lastName : '',
  //     email : '',
  //     company : '',
  //     country : ''
  //   }
  //   this.handleFirst = this.handleFirst.bind(this);
  //   this.handleLast = this.handleLast.bind(this);
  //   this.handleEmail = this.handleEmail.bind(this);
  //   this.handleCompany = this.handleCompany.bind(this);
  //   this.handleCountry = this.handleCountry.bind(this);
  // }

  // handleFirst = (e) => {
  //   this.setState({ firstName: text })
  // }
  // handleLast = (text) => {
  //   this.setState({ lastName: text })
  // }
  // handleEmail = (text) => {
  //   this.setState({ email: text })
  // }
  // handleCompany = (text) => {
  //   this.setState({ company: text })
  // }
  // handleCountry = (text) => {
  //   this.setState({ country: text })
  // }

  // klikPost() {
  //   if (this.state.firstName && this.state.lastName && this.state.email && this.state.company && this.state.country ) {
  //   const user = {
  //     firstName: this.state.firstName,
  //     lastName: this.state.lastName,
  //     email: this.state.email,
  //     company: this.state.company,
  //     country: this.state.country
  //   } 
  //   var config = {
  //     headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  //     responseType: 'blob'
  //   }; 

  // 
    // var url = 'https://youdsqubisa123.herokuapp.com/user'
    // var url = 'http://localhost:5000/user'
    // axios.post(url, qs.stringify(user), config)
    //   .then((res) => {
    //     alert("Sign Up Success");
    //     // this.props.navigation.jumpTo('')
    //     console.log(res.data);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   })
    // console.log(this.state);
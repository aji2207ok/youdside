import React, { Component } from "react";
import EmbedSDK from "@mongodb-js/charts-embed-dom";

class ExpressionsDiagram extends Component {
  loadchart() {
    const sdk = new EmbedSDK({
      baseUrl: "https://charts.mongodb.com/charts-qubisa_youds-ytnrd",
    });

    const chart = sdk.createChart({
      chartId: "407fa9be-af90-4657-8453-04606fdca941",
      // width: 680,
      height: 500,
      theme: "dark",
    });

    chart.render(document.getElementById("chart"));
  }
  //   loadchart();
  componentDidMount() {
    this.loadchart();
  }
  render() {
    return (
      <center>
        <div width="80%" id="chart"></div>
      </center>
    );
  }
}

export default ExpressionsDiagram;

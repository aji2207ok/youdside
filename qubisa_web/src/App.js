import "./App.css";
import Container from "./component/container";
import Containeraction from "./component/containeraction";
import About from "./component/about";
import Footer from "./component/footer";
import Header from "./component/header";
// import Welcome from './component/welcome';
import Body from "./component/body";
import Contact from "./component/contact";
import Demo from "./component/demo";
import Services from "./component/services";
import Opencamera from "./component/opencamera";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ExpressionsDiagram from "./component/expressionsDiagram";
import Signup from "./component/signup";

function App() {
  return (
    <Router>
      <div
        className="parallax-window"
        data-parallax="scroll"
        data-image-src="img/bg-01.jpg"
      >
        <div className="container-fluid">
          <Header />
          <Route path="/" exact component={Body} />
          <Route path="/services" component={Services} />
          <Route path="/expressionsDiagram" component={ExpressionsDiagram} />
          <Route path="/opencamera" component={Opencamera} />
          <Route path="/signup" component={Signup} />
        </div>
      </div>
    </Router>
  );
}

export default App;

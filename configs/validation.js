const Joi = require('@hapi/joi')

const registerValidation = (data) => {
    const schema = Joi.object({
        // firtstname: Joi.string()
        //     .required(),
        email: Joi.string()
            .email()
            .required(),
    })

    return schema.validate(data)
}

module.exports.registerValidation = registerValidation